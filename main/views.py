from django.views import View
from django.views.generic import TemplateView


class HomeView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        return {
            "home": "Home page"
        }


class AboutView(TemplateView):
    template_name = "about.html"

    def get_context_data(self, **kwargs):
        return {
            "about": "About page"
        }


class ContactsView(TemplateView):
    template_name = "contacts.html"

    def get_context_data(self, **kwargs):
        return {
            "contacts": "Contacts page"
        }


class PortfolioView(TemplateView):
    template_name = "portfolio.html"

    def get_context_data(self, **kwargs):
        return None